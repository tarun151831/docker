FROM tomcat:8.0
MAINTAINER tarun
ADD https://gitlab.com/tarun151831/docker.git /usr/local/tomcat/webapps/
ADD https://gitlab.com/tarun151831/docker.git /usr/local/tomcat/conf
ADD https://gitlab.com/tarun151831/docker.git /usr/local/tomcat/conf
EXPOSE 8080
USER root
WORKDIR /usr/local/tomcat/webapps/
CMD ["catalina.sh","run"]
